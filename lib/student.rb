class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{self.first_name} #{self.last_name}"
  end

  def enroll(course)
    unless self.courses.include?(course)
      self.courses.push(course)
      course.students.push(self)
    end
  end

  def course_load
    course_hash = Hash.new(0)
    courses.each do |course|
      course_hash[course.department] += course.credits
    end
    course_hash
  end
end
