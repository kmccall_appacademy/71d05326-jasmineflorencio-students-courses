class Course
  attr_accessor :name, :department, :credits, :students

  def initialize(name, department, credits)
    @name = name
    @department = department
    @credits = credits
    @students = []
  end

  def add_student(student)
    student.enroll(self)
  end

end
